package main

import "fmt"
import "syscall"
import "log"
import "os"
import "io/ioutil"
import "os/exec"
import "os/user"
import "strconv"
import "path"
import "flag"

func list_dir(path string) {
    files, _ := ioutil.ReadDir(path)
    for _, f := range files {
        fmt.Println(f.Name())
    }
}

func mount_bind(target string, mount_point string) {

    if err := syscall.Mount(mount_point, path.Join(target, mount_point), "", syscall.MS_BIND, ""); err != nil {
        log.Fatal(err, ": ", path.Join(target, mount_point))
    }
}

func mount_rbind(target string, mount_point string) {

    if err := syscall.Mount(mount_point, path.Join(target, mount_point), "", syscall.MS_REC | syscall.MS_BIND, ""); err != nil {
        log.Fatal(err)
    }
}

func unmount(target string, mount_point string) {
    if err := syscall.Unmount(path.Join(target, mount_point), syscall.MNT_DETACH); err != nil {
        log.Fatal(err, ": ", path.Join(target, mount_point))
    }
}

func main() {

    log.SetFlags(log.LstdFlags | log.Lshortfile)

    target := "/mnt"
    flag.StringVar(&target, "target", target, "mount point")
    lowerdir := flag.String("lower", "/", "lowerdir")
    upperdir := flag.String("upper", "upper", "upperdir")
    workdir := flag.String("work", "work", "workdir")
    flag.Parse()

    src := "gum"
    fstype := "overlay"
    options := "lowerdir=" + *lowerdir + ",upperdir=" + *upperdir + ",workdir=" + *workdir

    current_user, err := user.Current()

    dir, err := os.Getwd()
    if err != nil {
        log.Fatal(err)
    }

    if err := syscall.Mount(src, target, fstype, 0, options); err != nil {
        log.Fatal(err)
    }

    defer func() {
        if err := syscall.Unmount(target, 0); err != nil {
            log.Fatal(err)
        }
    }()

    if err := syscall.Mount("/proc", path.Join(target, "proc"), "proc", 0, ""); err != nil {
        log.Fatal(err, ": ", path.Join(target, "proc"))
    }
    defer unmount(target, "/proc")

    if err := syscall.Mount("/sys", path.Join(target, "sys"), "sysfs", 0, ""); err != nil {
        log.Fatal(err, ": ", path.Join(target, "sys"))
    }
    defer unmount(target, "/sys")

    mount_bind(target, "/dev")
    defer unmount(target, "/dev")

    mount_bind(target, "/dev/pts")
    defer unmount(target, "/dev/pts")

    mount_bind(target, "/dev/shm")
    defer unmount(target, "/dev/shm")

    mount_bind(target, "/run")
    defer unmount(target, "/run")

    mount_bind(target, path.Join("/run/user", current_user.Uid))
    defer unmount(target, path.Join("/run/user", current_user.Uid))

    rootfs, err := syscall.Open("/", 0, 0)
    if err != nil {
        log.Fatal(err)
    }

    defer func() {
        if err := syscall.Close(rootfs); err != nil {
            log.Fatal(err)
        }
    }()

    if err := syscall.Chroot(target); err != nil {
        log.Fatal(err)
    }

    defer func() {
        if err := syscall.Fchdir(rootfs); err != nil {
            log.Fatal(err)
        }

        if err := syscall.Chroot("."); err != nil {
            log.Fatal(err)
        }

        if err := os.Chdir(dir); err != nil {
            log.Fatal(err)
        }
    }()

    uid, err := strconv.Atoi(current_user.Uid)
    gid, err := strconv.Atoi(current_user.Gid)

    cmd_arg := os.Getenv("SHELL")

    if len(flag.Args()) >= 1 {
        cmd_arg = flag.Arg(0)
    }

    cmd := exec.Command("script", "-qfc", cmd_arg)
    cmd.Stdout = os.Stdout
    cmd.Stdin = os.Stdin
    cmd.Stderr = os.Stderr
    cmd.Dir = dir
    env := os.Environ()
    env = append(env, fmt.Sprintf("GUM=%s", *upperdir))
    cmd.Env = env
    cmd.SysProcAttr = &syscall.SysProcAttr{
        Credential: &syscall.Credential{
            Uid: uint32(uid),
            Gid: uint32(gid),
        },
        Setsid: true,
    }

    cmd.Run()
}
