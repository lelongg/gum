# Gum

## Installation

    git clone git@gitlab.com:lelongg/gum.git
    cd ./gum
    sudo groupadd gum
    sudo usermod -a -G gum <username>
    sudo make install

## Quickstart

    gump

You are now in a sandbox environment.
Type ctrl+D to exit.

## Synopsis

usage: gump [-h] [-l] [gumballs [gumballs ...]]

positional arguments:
  gumballs    the gumballs to stack

optional arguments:
  -h, --help  show this help message and exit
  -l, --list  show existing gumballs

## Usage

List available gumballs.

    gump -l

Jump to a gumball named *foo* (it will be created if not existing).

    gump foo

Jump to a gumball named *bar* with *foo* stacked underneath.

    gump foo bar

Gumballs created on a stack will remember this stack on next jump.

## Remarks

Gumballs are fetched from and saved to the directory defined by `$GUMDIR` (`~/.gumdir` otherwise).

Temporary directories are created in the directory defined by `$GUMTMP` (`/tmp/gum` otherwise).

Initial gumballs stacks are saved inside each gumball in a `.gum` file.
