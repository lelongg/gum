gum: gum.go
	go build -o gum && chown root:gum ./gum && chmod 4754 ./gum

install: gum gump
	cp ./gum /usr/local/bin/gum && chown root:gum /usr/local/bin/gum && chmod 4754 /usr/local/bin/gum
	cp ./gump /usr/local/bin/gump
